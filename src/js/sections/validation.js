//*Форма 1
const nameInput = document.getElementById('name');
const surnameInput = document.getElementById('surname');
const emailInput = document.getElementById('email');
const phoneInput = document.getElementById('phone');

nameInput.addEventListener('input', validateName);
surnameInput.addEventListener('input', validateSurname);
emailInput.addEventListener('input', validateEmail);
phoneInput.addEventListener('input', validatePhone);

function validateName() {
	const nameValue = nameInput.value;
	const nameError = document.querySelector('.forma__name .error');

	if (/^[A-ZА-Я][a-zа-я]{2,}$/.test(nameValue)) {
		nameInput.style.borderColor = '';
		if (nameError) {
			nameError.remove();
		}
	} else {
		nameInput.style.borderColor = 'red';
		if (!nameError) {
			nameInput.insertAdjacentHTML(
				"afterend",
				`<span class="error">*Wpisz swoje imię</span>`
			);
		}
	}
}

function validateSurname() {
	const surnameValue = surnameInput.value;
	const surnameError = document.querySelector('.forma__surname .error');

	if (/^[A-ZА-Я][a-zа-я]{2,}$/.test(surnameValue)) {
		surnameInput.style.borderColor = '';
		if (surnameError) {
			surnameError.remove();
		}
	} else {
		surnameInput.style.borderColor = 'red';
		if (!surnameError) {
			surnameInput.insertAdjacentHTML(
				"afterend",
				`<span class="error">*Wpisz swoje nazwisko</span>`
			);
		}
	}
}

function validateEmail() {
	const emailValue = emailInput.value;
	const emailError = document.querySelector('.forma__email .error');

	if (/^\S+@\S+\.\S+$/.test(emailValue)) {
		emailInput.style.borderColor = '';
		if (emailError) {
			emailError.remove();
		}
	} else {
		emailInput.style.borderColor = 'red';
		if (!emailError) {
			emailInput.insertAdjacentHTML(
				"afterend",
				`<span class="error">*Wpisz prawidłowy adres E-mail</span>`
			);
		}
	}
}

function validatePhone() {
	const phoneValue = phoneInput.value;
	const phoneError = document.querySelector('.forma__phone .error');

	if (/^\d+$/.test(phoneValue)) {
		phoneInput.style.borderColor = '';
		if (phoneError) {
			phoneError.remove();
		}
	} else {
		phoneInput.style.borderColor = 'red';
		if (!phoneError) {
			phoneInput.insertAdjacentHTML(
				"afterend",
				`<span class="error">*Wpisz swój numer telefonu</span>`
			);
		}
	}
}

const btnForm1 = document.querySelector('.forma__btn')
const form = document.getElementById('forma');
btnForm1.addEventListener('click', function (e) {
	
	validateName();
	validateSurname();
	validateEmail();
	validatePhone();

	const hasError = (
		nameInput.style.borderColor === 'red' ||
		surnameInput.style.borderColor === 'red' ||
		emailInput.style.borderColor === 'red' ||
		phoneInput.style.borderColor === 'red'
	);

	const isEmpty = (
		nameInput.value.trim() === '' ||
		surnameInput.value.trim() === '' ||
		emailInput.value.trim() === '' ||
		phoneInput.value.trim() === ''
	);

	if (hasError || isEmpty) {
		e.preventDefault();
	} else {
	form.submit()
	}
});

//*Форма2
const nameInput2 = document.getElementById('name2');
const surnameInput2 = document.getElementById('surname2');
const emailInput2 = document.getElementById('email2');
const phoneInput2 = document.getElementById('phone2');

nameInput2.addEventListener('input', validateName2);
surnameInput2.addEventListener('input', validateSurname2);
emailInput2.addEventListener('input', validateEmail2);
phoneInput2.addEventListener('input', validatePhone2);

function validateName2() {
	const nameValue = nameInput2.value;
	const nameError = document.querySelector('.form__name .error2');

	if (/^[A-ZА-Я][a-zа-я]{2,}$/.test(nameValue)) {
		nameInput2.style.borderColor = '';
		if (nameError) {
			nameError.remove();
		}
	} else {
		nameInput2.style.borderColor = 'red';
		if (!nameError) {
			nameInput2.insertAdjacentHTML(
				"afterend",
				`<span class="error2">*Wpisz swoje imię</span>`
			);
		}
	}
}

function validateSurname2() {
	const surnameValue = surnameInput2.value;
	const surnameError = document.querySelector('.form__surname .error2');

	if (/^[A-ZА-Я][a-zа-я]{2,}$/.test(surnameValue)) {
		surnameInput2.style.borderColor = '';
		if (surnameError) {
			surnameError.remove();
		}
	} else {
		surnameInput2.style.borderColor = 'red';
		if (!surnameError) {
			surnameInput2.insertAdjacentHTML(
				"afterend",
				`<span class="error2">*Wpisz swoje nazwisko</span>`
			);
		}
	}
}

function validateEmail2() {
	const emailValue = emailInput2.value;
	const emailError = document.querySelector('.form__email .error2');

	if (/^\S+@\S+\.\S+$/.test(emailValue)) {
		emailInput2.style.borderColor = '';
		if (emailError) {
			emailError.remove();
		}
	} else {
		emailInput2.style.borderColor = 'red';
		if (!emailError) {
			emailInput2.insertAdjacentHTML(
				"afterend",
				`<span class="error2">*Wpisz prawidłowy adres E-mail</span>`
			);
		}
	}
}

function validatePhone2() {
	const phoneValue = phoneInput2.value;
	const phoneError = document.querySelector('.form__phone .error2');

	if (/^\d+$/.test(phoneValue)) {
		phoneInput2.style.borderColor = '';
		if (phoneError) {
			phoneError.remove();
		}
	} else {
		phoneInput2.style.borderColor = 'red';
		if (!phoneError) {
			phoneInput2.insertAdjacentHTML(
				"afterend",
				`<span class="error2">*Wpisz swój numer telefonu</span>`
			);
		}
	}
}

const btnForm2 = document.querySelector('.form__btn')
const form2 = document.getElementById('form');
btnForm2.addEventListener('click', function (e) {

	validateName2();
	validateSurname2();
	validateEmail2();
	validatePhone2();

	const hasError = (
		nameInput2.style.borderColor === 'red' ||
		surnameInput2.style.borderColor === 'red' ||
		emailInput2.style.borderColor === 'red' ||
		phoneInput2.style.borderColor === 'red'
	);

	const isEmpty = (
		nameInput2.value.trim() === '' ||
		surnameInput2.value.trim() === '' ||
		emailInput2.value.trim() === '' ||
		phoneInput2.value.trim() === ''
	);

	if (hasError || isEmpty) {
		e.preventDefault();
	} else {
		form2.submit()
	}
});