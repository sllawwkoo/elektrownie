const navMenu = document.querySelector('.menu');
navMenu.addEventListener('click', function (e) {
	if (e.target.classList.contains('menu__link')) {
		let link = e.target;
		e.preventDefault();
		window.scrollTo({
			top: document.querySelector(link.hash).offsetTop,
			behavior: "smooth"
		});
	}
});