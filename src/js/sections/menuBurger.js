const iconBurger = document.querySelector('.burger');
const menuBody = document.querySelector('.menu');
const body = document.body;

if (iconBurger) {
	document.body.addEventListener('click', (e) => {
		const navMenu = e.composedPath().includes(menuBody);

		if (e.target.closest('.burger') || e.target.closest('.menu__link')) {
			iconBurger.classList.toggle('burger--active');
			menuBody.classList.toggle('menu--active');

			// Додаємо клас 'lock' лише якщо iconBurger має клас 'burger--active'
			if (iconBurger.classList.contains('burger--active')) {
				body.classList.add('lock');
			} else {
				body.classList.remove('lock');
			}
		} else if (!navMenu) {
			e.preventDefault();
			menuBody.classList.remove('menu--active');
			iconBurger.classList.remove('burger--active');
			body.classList.remove('lock');
		}
	})
}