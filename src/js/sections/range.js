const rangeSlider = document.getElementById('slider-range');
const rangeAmount = document.querySelector('.range__amount');

noUiSlider.create(rangeSlider, {
	start: [1100],
	// connect:[true, false],
	range: {
		'min': [1100],
		'max': [500000]
	},
	tooltips: [
		true,
	],
});

rangeSlider.noUiSlider.on('update', function (values, handle) {
	
	const sliderValue = parseInt(values[0]);
	const procent = (sliderValue * 1.2).toFixed(0);
	
	rangeAmount.textContent = procent + ' PLN';
});




