// Инициализируем Swiper
let myImageSlider = new Swiper('.swiper', {
	// Стрелки
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev'
	},
	// Навигация 
	// Буллеты, текущее положение, прогрессбар
	pagination: {
		el: '.swiper-pagination',
		
		// Буллеты
		type: 'bullets',
		clickable: true,
		// Динамические буллеты
		// dynamicBullets: true,
		// Кастомные буллеты
	}
})