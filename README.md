### Deploy  [Landing Page Elektrownie](https://sllawwkoo.github.io/elektrownie_deploy/)
***

#### Використані технології та інструменти:
*  Методологія BEM
*  HTML, CSS, JavaScript 
*  Препрцесор Sass SCSS
*  Gulp для автоматизації рутинних задач розробки;
*  Git та GitLab для контролю версій коду;
***

#### Технічні моменти:
__1.__ Запуск проекту в режимі ___development___ здійснюється за допомогою номанди - ``` npm run dev ```

__2.__ Запуск проекту в режимі ___production___ здійснюється за допомогою номанди - ``` npm run build ```

__3.__ В папці  ___dist___ знаходиться кінцевий результат збірки даного проекту.
 
__4.__ [Посилання на сторінку виконаного проекту ](https://sllawwkoo.github.io/elektrownie_deploy/)
